spatialite-gui (2.1.0~beta1-3) unstable; urgency=medium

  * Add Rules-Requires-Root to control file.
  * Bump Standards-Version to 4.7.0, no changes.
  * Bump debhelper compat to 13.
  * Enable Salsa CI.
  * Replace pkg-config build dependency with pkgconf.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 07 Mar 2025 08:19:14 +0100

spatialite-gui (2.1.0~beta1-2) unstable; urgency=medium

  * Update lintian overrides.
  * Bump watch file version to 4.
  * Bump Standards-Version to 4.6.1, no changes.
  * Update upstream metadata.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Drop obsolete dh_strip override, dbgsym migration complete.
  * Update build dependencies for wxwidgets3.2.
    (closes: #1019775)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 25 Oct 2022 05:16:05 +0200

spatialite-gui (2.1.0~beta1-1) unstable; urgency=medium

  * Bump libspatialite-dev version requirement to 5.0.0.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 Aug 2020 06:44:22 +0200

spatialite-gui (2.1.0~beta1-1~exp2) experimental; urgency=medium

  * Bump libspatialite-dev version requirement to 5.0.0~rc1.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 03 Aug 2020 12:04:42 +0200

spatialite-gui (2.1.0~beta1-1~exp1) experimental; urgency=medium

  * New upstream beta release.
  * Bump Standards-Version to 4.5.0, no changes.
  * Drop Name field from upstream metadata.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
    - Don't explicitly enable autoreconf, enabled by default
    - Drop dh-autoreconf build dependency
  * Update copyright file.
  * Bump librasterlite2-dev version requirement to 1.1.0~beta1.
  * Drop patches applied upstream. Refresh remaining patch.
  * Drop -DACCEPT_USE_OF_DEPRECATED_PROJ_API_H, proj.h is supported now.
  * Add libminizip-dev to build dependencies.
  * Add liblz4-dev to build dependencies.
  * Add libzstd-dev to build dependencies.
  * Disable XlsxWriter support.
  * Add patch to fix FTBFS without XlsxWriter support.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 03 Aug 2020 08:49:19 +0200

spatialite-gui (2.1.0~beta0+really2.1.0~beta0-1~exp5) experimental; urgency=medium

  * Switch to wxWidgets GTK 3 implementation.
    (closes: #933409)

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 16 Sep 2019 13:24:23 +0200

spatialite-gui (2.1.0~beta0+really2.1.0~beta0-1~exp4) experimental; urgency=medium

  * Define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H for PROJ 6.0.0 compatibility.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 11 Jul 2019 13:03:10 +0200

spatialite-gui (2.1.0~beta0+really2.1.0~beta0-1~exp3) experimental; urgency=medium

  * Bump Standards-Version to 4.3.0, no changes.
  * Disable CharLS support, version 2.0.0 is not supported yet.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 11 Jan 2019 13:28:03 +0100

spatialite-gui (2.1.0~beta0+really2.1.0~beta0-1~exp2) experimental; urgency=medium

  * Bump Standards-Verson to 4.2.1, no changes.
  * Fix required version of librasterlite2-dev.
    (closes: #908479)

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 10 Sep 2018 18:06:41 +0200

spatialite-gui (2.1.0~beta0+really2.1.0~beta0-1~exp1) experimental; urgency=medium

  * Revert back to 2.1.0~beta0-1~exp1.
  * Require at least libspatialite-dev 5.0.0~.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Aug 2018 23:22:16 +0200

spatialite-gui (2.1.0~beta0+really2.0.0~devel2-5) unstable; urgency=medium

  * Switch to wxWidgets GTK 3 implementation.
    (closes: #933409)

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 16 Sep 2019 13:24:58 +0200

spatialite-gui (2.1.0~beta0+really2.0.0~devel2-4) unstable; urgency=medium

  * Define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H for PROJ 6.0.0 compatibility.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 11 Jul 2019 12:23:24 +0200

spatialite-gui (2.1.0~beta0+really2.0.0~devel2-3) unstable; urgency=medium

  * Disable CharLS support, version 2.0.0 is not supported yet.
  * Bump Standards-Version to 4.3.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 11 Jan 2019 13:19:21 +0100

spatialite-gui (2.1.0~beta0+really2.0.0~devel2-2) unstable; urgency=medium

  * Bump Standards-Verson to 4.2.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 04 Aug 2018 11:33:57 +0200

spatialite-gui (2.1.0~beta0+really2.0.0~devel2-1) unstable; urgency=medium

  * Revert back to 2.0.0~devel2-10.
  * Remove .gitignore file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Aug 2018 23:06:16 +0200

spatialite-gui (2.1.0~beta0-1~exp1) unstable; urgency=medium

  * New upstream beta release.
  * Update copyright years for Alessandro Furieri.
  * Add curl & pq build dependencies.
  * Add libvirtualpg-dev to build dependencies.
  * Drop patches applied upstream. Refresh remaining patch.
  * Don't install icons manually, fixed upstream.
  * Require at least librasterlite2-dev 1.1.0~.
  * Add patch link libz for compress.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Aug 2018 07:13:49 +0200

spatialite-gui (2.0.0~devel2-10) unstable; urgency=medium

  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 19:51:22 +0200

spatialite-gui (2.0.0~devel2-9) unstable; urgency=medium

  * Update copyright-format URL to use HTTPS.
  * Bump Standards-Verson to 4.1.5, no changes.
  * Update watch file to use HTTPS.
  * Update Vcs-* URLs for Salsa.
  * Strip trailing whitespace from control & rules files.
  * Drop obsolete dbg package.
  * Update packaging for single binary package.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 Jul 2018 21:11:09 +0200

spatialite-gui (2.0.0~devel2-8) unstable; urgency=medium

  * Add patch to fix FTBFS with GCC 7.
    (closes: #853667)
  * Don't explicitly build with GCC 6.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 Oct 2017 19:20:00 +0200

spatialite-gui (2.0.0~devel2-7) unstable; urgency=medium

  * Add patch to fix FTBFS with OpenJPEG 2.3.
  * Bump Standards-Verson to 4.1.1, no changes.
  * Add lintian override for debian-watch-uses-insecure-uri.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 Oct 2017 17:02:13 +0200

spatialite-gui (2.0.0~devel2-6) unstable; urgency=medium

  * Change priority from extra to optional.
  * Bump Standards-Version to 4.1.0, changes: priority.
  * Add patch to fix FTBFS with OpenJPEG 2.2.
    (closes: #876809)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 26 Sep 2017 13:55:38 +0200

spatialite-gui (2.0.0~devel2-5) unstable; urgency=medium

  * Explicitly build with GCC 6 (FTBFS with GCC 7 not fixed yet).
  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 05 Aug 2017 13:15:59 +0200

spatialite-gui (2.0.0~devel2-4) unstable; urgency=medium

  * Add patch to also install 22x22, 36x36 & 42x42 icons in hicolor theme.
  * Bump Standards-Version to 3.9.8, changes: hicolor icons.
  * Enable parallel builds.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 May 2016 21:21:46 +0200

spatialite-gui (2.0.0~devel2-3) unstable; urgency=medium

  * Update Vcs-Git URL to use HTTPS.
  * Use desktop file in favor of menu file per CTTE #741573.
  * Install application icon in hicolor theme.
  * Bump Standards-Version to 3.9.7, no changes.
  * Add patch to install application icon with autotools.
  * Enable all hardening buildflags.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 12 Mar 2016 14:08:51 +0100

spatialite-gui (2.0.0~devel2-2) unstable; urgency=medium

  * Update Vcs-Browser URL to use HTTPS.
  * Override dh_install to use --list-missing.
  * Rebuild for geos transition.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 29 Aug 2015 16:19:08 +0200

spatialite-gui (2.0.0~devel2-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 22 Jul 2015 19:01:12 +0200

spatialite-gui (2.0.0~devel2-1~exp1) experimental; urgency=medium

  * New upstream pre-release.
  * Drop patches applied upstream.
  * Bump minimum required libspatialite-dev to 4.3.0.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Jul 2015 23:07:04 +0200

spatialite-gui (2.0.0~devel1-1~exp1) experimental; urgency=medium

  * New upstream pre-release.
  * Update copyright file, changes:
    - Update copyright years
    - Drop .0 from GPL license shortnames.
  * Drop patches applied upstream, refresh remaining patches.
  * Bump minimum required libspatialite-dev to 4.3.0~rc1.
  * Bump minimum required librasterlite2-dev to 1.0.0~rc0+devel.
  * Add patch to fix -Wformat & -Werror=format-security issues.
  * Update desktop file to use the %f Exec variable for a single file name.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 27 Jun 2015 22:31:17 +0200

spatialite-gui (1.7.1-6) unstable; urgency=medium

  * Add upstream metadata.
  * Update Vcs-Browser URL to use cgit instead of gitweb.
  * Add license & copyright for Silk icons.
  * Update my email to @debian.org address.
  * Bump Standards-Version to 3.9.6, no changes.
  * Also support -devel pre-releases in watchfile uversionmangle.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 19 Jun 2015 23:03:08 +0200

spatialite-gui (1.7.1-5) unstable; urgency=medium

  [ Ross Gammon ]
  * Team upload.
  * Add patch to fix crash when loading shapefiles.
    Thanks to Alessandro Furieri (Closes: #761629)
  * Add .gitignore file to ignore quilt files

  [ Bas Couwenberg ]
  * Reorder and refrash patches.

 -- Ross Gammon <rossgammon@mail.dk>  Thu, 02 Oct 2014 22:00:47 +0200

spatialite-gui (1.7.1-4) unstable; urgency=medium

  [ Olly Betts ]
  * Update to use wxWidgets 3.0.
    (closes: #748085)

  [ Bas Couwenberg ]
  * Drop lintian override for no-upstream-changelog,
    shouldn't override pedantic tags.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Thu, 29 May 2014 14:31:52 +0200

spatialite-gui (1.7.1-3) unstable; urgency=low

  * Add gbp.conf to use pristine-tar by default.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Mon, 17 Mar 2014 23:12:25 +0100

spatialite-gui (1.7.1-2) experimental; urgency=low

  * Bump Standards-Version to 3.9.5, no changes required.
  * Require at least librasterlite version 1.1g-3.
  * Require at least libspatialite version 4.1.1-5 for libxml2 support.
  * Re-enable building with libxml2 support.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sat, 14 Dec 2013 17:43:39 +0100

spatialite-gui (1.7.1-1) experimental; urgency=low

  * New upstream version. (closes: #688328)
  * Add myself to Uploaders.
  * Update watch file, incorporate sepwatch changes and handle common mistakes.
  * Update Homepage URL.
  * Use canonical URLs in Vcs-* fields.
  * Update minimum libspatialite version to 4.1.0.
  * Update minimum libgaiagraphics version to 0.5.
  * Add build dependency on libxml2-dev.
  * Add patch to link properly link sqlite3.
  * Disable libxml2 support, libgaiagraphics doesn't support the functions yet.
  * Add lintian override for no upstream changelog, link to timeline.
  * Add lintian override for missing man page, not useful for GUI program.
  * Add Keywords entry to desktop file.
  * Update copyright file.
  * Change priority to optional, keep priority extra for -dbg package.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Mon, 30 Sep 2013 19:30:46 +0200

spatialite-gui (1.6.0-1) experimental; urgency=low

  * New upstream version
  * Bump requirement on libspatialite to >= 4.0.0

 -- David Paleino <dapal@debian.org>  Sat, 01 Dec 2012 21:55:23 +0100

spatialite-gui (1.5.0-1) experimental; urgency=low

  * New upstream version (Closes: #689192)
  * Add missing Build-Depends on libgaiagraphics-dev and libfreexl-dev
  * Bumped debhelper compatibility to 9
  * Standards-Version bump to 3.9.4, no changes needed
  * Updated debian/copyright

 -- David Paleino <dapal@debian.org>  Sun, 30 Sep 2012 16:30:32 +0200

spatialite-gui (1.2.1-3) unstable; urgency=low

  * Fixed FTBS due to relinking in Makefile-linux and the new geos lib.
    (closes: #663646)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Tue, 13 Mar 2012 00:20:05 +0100

spatialite-gui (1.2.1-2) unstable; urgency=low

  * Fix package description, be more verbose.

 -- David Paleino <dapal@debian.org>  Wed, 23 Mar 2011 21:38:27 +0100

spatialite-gui (1.2.1-1) unstable; urgency=low

  * Initial release (Closes: #610998)

 -- David Paleino <dapal@debian.org>  Mon, 24 Jan 2011 19:45:15 +0100
